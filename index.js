function IsCallable(func) {
    return func.call;
}

Object.prototype[Symbol.toPrimitive] = function(hint) {
    console.log(`Происходит преобразование ${hint}`);
    let toStr = '';
    let valOf = '';
    let tempObj ={};
    
    // if (!(this.toString && this.valueOf)) {
    //     throw new TypeError();
    // }

    if(!this.toString) {
        throw new TypeError();
    }

    if (!(hint === 'string')) {
        valOf = this.valueOf();
        if (!IsCallable(valOf) && typeof valOf !== 'object') {
            return valOf;
        } else {
            tempObj = valOf; 
        }
        toStr = valOf.toString();
        if (!IsCallable(toStr) && typeof toStr !== 'object') {
             return toStr;
        } else {
            tempObj = toStr;  
        }
    } 
    toStr = this.toString();
    if (!IsCallable(toStr) && typeof toStr !== 'object' && !this.valueOf) {
        return toStr;
    } else {
        tempObj = toStr; 
    }
    valOf = tempObj.valueOf();
    if (!IsCallable(valOf) && typeof valOf !== 'object') {
         return valOf;
    } else {
         tempObj = valOf;
    }
    throw new SyntaxError();
}

let car = {
    toString() {
        return undefined;
    }
}

// console.log([] + {} + [2] + {})
// console.log({} > [])
// console.log({} + [100])
// console.log(6/'3')